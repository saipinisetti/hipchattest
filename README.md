# README #

HipChat Austin team test submission


### How do I get set up? ###

* This is a maven project
* To build run **mvn clean package**
* The above will produce an independent jar file test-1.0-SNAPSHOT.jar
* **java -jar target/test-1.0-SNAPSHOT.jar server ./src/local.yml**

### To Test ###

* curl localhost:8080/v1/messages -XPOST -H"Content-Type:text/plain"  --data "Hi @bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
* You should see something like: {"mentions":["bob","john"],"emoticons":["success"],"links":[{"url":"https://twitter.com/jdorfman/status/430511497475670016","title":"Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"}]}
