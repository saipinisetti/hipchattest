package com.atlas.hipchat.service;

import com.atlas.hipchat.model.Link;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static java.util.concurrent.TimeUnit.DAYS;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class HtmlTitleParserTest {

    HtmlTitleParser htmlTitleParser;
    Cache<String, String> cacheMock = mock(Cache.class);
    Cache<String, String> cache;
    @Before
    public void setUp() throws Exception {
        cache =
                CacheBuilder.newBuilder()
                        .maximumSize(10)
                        .expireAfterAccess(1, DAYS)
                        .build();

        htmlTitleParser = new HtmlTitleParser(cacheMock);
    }

    @Test
    public void applyFromCache() throws Exception {

        Link link = new Link("http://www.hipchat.com","");
        doReturn("HipChat Title").when(cacheMock).get(any(), any());
        Link linkWithTitle = htmlTitleParser.apply(link);

        Assert.assertEquals("HipChat Title", linkWithTitle.getTitle());
    }

    @Test
    public void applyFromWeb() throws Exception {

        htmlTitleParser = new HtmlTitleParser(cache);
        Link link = new Link("http://www.hipchat.com","");
        Link linkWithTitle = htmlTitleParser.apply(link);

        Assert.assertEquals("HipChat | Team Group Chat, Video Chat &amp; Screen Sharing", linkWithTitle.getTitle());
    }

}