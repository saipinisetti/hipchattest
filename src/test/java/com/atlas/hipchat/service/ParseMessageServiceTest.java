package com.atlas.hipchat.service;

import com.atlas.hipchat.model.Link;
import com.atlas.hipchat.model.MessageMeta;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class ParseMessageServiceTest {

    ParseMessageService parseMessageService;
    HtmlTitleParser htmlTitleParser;
    @Before
    public void setUp() throws Exception {

        htmlTitleParser = mock(HtmlTitleParser.class);
        parseMessageService = new ParseMessageService(htmlTitleParser);
    }

    @Test
    public void parseWithMentions() throws Exception {
        MessageMeta messageMeta = parseMessageService.parse("Hi @sai");
        assertNull(messageMeta.getEmoticons());
        assertNull(messageMeta.getLinks());
        assertTrue(messageMeta.getMentions().contains("sai"));
        Assert.assertEquals(1,messageMeta.getMentions().size());
    }

    @Test
    public void parseWithEmoticons() throws Exception {
        MessageMeta messageMeta = parseMessageService.parse("Hi (happy) and");
        assertNull(messageMeta.getMentions());
        assertNull(messageMeta.getLinks());
        assertTrue(messageMeta.getEmoticons().contains("happy"));
        Assert.assertEquals(1,messageMeta.getEmoticons().size());
    }

    @Test
    public void parseWithLinks() throws Exception {
        doReturn(new Link("http://www.hipchat.com","HipChat Title")).when(htmlTitleParser).apply(any(Link.class));
        MessageMeta messageMeta = parseMessageService.parse("Hi http://www.hipchat.com");
        assertNull(messageMeta.getEmoticons());
        assertNull(messageMeta.getMentions());
        assertTrue(messageMeta.getLinks().contains(new Link("http://www.hipchat.com","HipChat Title")));
        Assert.assertEquals(1,messageMeta.getLinks().size());
    }

    @Test
    public void parseWithNoMeta() throws Exception {
        MessageMeta messageMeta = parseMessageService.parse("Hi I dont have any meta.");
        assertNull(messageMeta.getEmoticons());
        assertNull(messageMeta.getMentions());
        assertNull(messageMeta.getLinks());
    }

    @Test
    public void parseWithAllMetaOnce() throws Exception {
        doReturn(new Link("http://www.hipchat.com","HipChat Title")).when(htmlTitleParser).apply(any(Link.class));
        MessageMeta messageMeta = parseMessageService.parse("Hi @sai i am (happy) to share the link http://www.hipchat.com");
        assertTrue(messageMeta.getMentions().contains("sai"));
        assertTrue(messageMeta.getEmoticons().contains("happy"));
        assertTrue(messageMeta.getLinks().contains(new Link("http://www.hipchat.com","HipChat Title")));
    }

    @Test
    public void parseWithAllMetaMulti() throws Exception {
        doReturn(new Link("http://www.hipchat.com","HipChat Title")).when(htmlTitleParser).apply(any(Link.class));
        MessageMeta messageMeta = parseMessageService.parse("Hi @sai i am (happy) to share the link http://www.hipchat.com with @chris");
        assertTrue(messageMeta.getMentions().contains("sai"));
        assertTrue(messageMeta.getMentions().contains("chris"));
        assertTrue(messageMeta.getEmoticons().contains("happy"));
        assertTrue(messageMeta.getLinks().contains(new Link("http://www.hipchat.com","HipChat Title")));
    }

    @Test
    public void parseWithEmptyMessage() throws Exception {
        MessageMeta messageMeta = parseMessageService.parse("");
        assertNull(messageMeta.getEmoticons());
        assertNull(messageMeta.getMentions());
        assertNull(messageMeta.getLinks());
    }

}