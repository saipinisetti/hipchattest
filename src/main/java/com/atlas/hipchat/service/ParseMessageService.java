package com.atlas.hipchat.service;

import com.atlas.hipchat.model.Link;
import com.atlas.hipchat.model.MessageMeta;
import com.google.inject.Inject;
import javafx.print.Collation;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ParseMessageService {

    private HtmlTitleParser htmlTitleParser;

    @Inject
    public ParseMessageService(HtmlTitleParser htmlTitleParser) {
        this.htmlTitleParser = htmlTitleParser;
    }

    public MessageMeta parse(String message) {

        //https://mathiasbynens.be/demo/url-regex :: this only extract urls starting with http/https
        final String REGEX = "\\B@\\w+|\\(\\w{0,15}\\)|(https?)://(-\\.)?([^\\s/?\\.#-]+\\.?)+(/[^\\s]*)?";

        Pattern pattern = Pattern.compile(REGEX);

        Matcher matcher = pattern.matcher(message);

        Set<String> items = new HashSet<>();

        while(matcher.find()) {
            String d = matcher.group();
            items.add(d);
        }

        // below groups based on the first char. e.g., @ for mentions and ( for emoticons -
        // cleaning up the special chars as target response not require them
        Map<String, Set<String>> d = items.stream()
                .filter(x -> x.startsWith("@") || x.startsWith("("))
                .collect(
                        Collectors.groupingBy(x -> x.substring(0,1),Collectors.mapping(x -> x.replaceAll("@|\\(|\\)",""),Collectors.toSet())
                        )
                );

        MessageMeta meta = new MessageMeta();
        meta.setMentions(d.get("@"));
        meta.setEmoticons(d.get("("));

        Set<Link> links = items.stream()
                .filter(x -> !x.startsWith("@") && !x.startsWith("("))
                .map( v -> new Link(v,""))
                .collect(Collectors.toSet());

        if(!links.isEmpty()) {
            Set<Link> withTitle = links.parallelStream()
                    .map(htmlTitleParser)
                    .collect(Collectors.toSet());

            meta.setLinks(withTitle);
        }
        return meta;
    }
}
