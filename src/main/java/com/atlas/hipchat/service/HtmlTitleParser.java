package com.atlas.hipchat.service;

import com.atlas.hipchat.model.Link;
import com.google.common.cache.Cache;
import com.google.inject.Inject;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Returns the HTML title of a web page and then cache with configured ttl. This class will stream the HTML content till it finds </title> tag
 */

@Slf4j
public class HtmlTitleParser implements Function<Link, Link>
{

    private static final String TTITLE_CLOSE = "</title>";

    Cache<String, String> titleCache;

    @Inject
    public HtmlTitleParser(Cache<String, String> cache) {
        this.titleCache = cache;
    }

    private String readTitle(String urlAsString) {

        String title = "";
        try {
            title = titleCache.get(urlAsString, () -> readTitleFromWeb(urlAsString));

        } catch (ExecutionException e) {
            log.error("Unable to load from cache", e.getMessage());
        }

        return title;
    }


    private String readTitleFromWeb(String urlAsString)
    {
        URL url;
        String title = "";
        String content = "";
        try {
            url = new URL(urlAsString);
            Scanner scan = new Scanner(url.openStream());

            while (scan.hasNext() && !content.contains(TTITLE_CLOSE)) {
                content += scan.nextLine();
            }
            scan.close();
        } catch (IOException e) {
            log.error("Unable to read title.", e.getMessage());
        }
        Pattern pattern = Pattern.compile("<title>(.*?)</title>");
        Matcher matcher = pattern.matcher(content);
        if(matcher.find()) {
            title = matcher.group(1);
        }

        return title;
    }

    @Override
    public Link apply(Link link) {
        return new Link(link.getUrl(), readTitle(link.getUrl()));
    }
}