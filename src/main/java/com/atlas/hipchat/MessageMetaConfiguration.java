package com.atlas.hipchat;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import lombok.Data;

@Data
public class MessageMetaConfiguration extends Configuration {
    @JsonProperty
    private String environment;

    @JsonProperty
    private String debugEnabled;

    @JsonProperty
    private Long cacheMaxSize;

    @JsonProperty
    private Long cacheDaysTtl;

}
