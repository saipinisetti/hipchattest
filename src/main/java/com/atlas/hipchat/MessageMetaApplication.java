package com.atlas.hipchat;

import com.atlas.hipchat.rest.MessageMetaResource;
import com.atlas.hipchat.service.HtmlTitleParser;
import com.atlas.hipchat.service.ParseMessageService;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import lombok.extern.slf4j.Slf4j;

import static java.util.concurrent.TimeUnit.DAYS;

/**
 *  Bootstrap class for Dropwizard application.
 */

@Slf4j
public class MessageMetaApplication extends Application<MessageMetaConfiguration>{

    private static final String NAME = "hipchatmeta";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void initialize(final Bootstrap<MessageMetaConfiguration> bootstrap) {
        super.initialize(bootstrap);
    }

    @Override
    public void run(MessageMetaConfiguration configuration, Environment environment) throws Exception {

        Cache<String, String> cache =
                CacheBuilder.newBuilder()
                        .maximumSize(configuration.getCacheMaxSize())
                        .expireAfterAccess(configuration.getCacheDaysTtl(), DAYS)
                        .build();

        final Injector injector = Guice.createInjector(

                new AbstractModule() {
                    @Override
                    protected void configure() {
                        bind(MessageMetaConfiguration.class).toInstance(configuration);
                        bind(Cache.class).toInstance(cache);
                        bind(HtmlTitleParser.class).toInstance(new HtmlTitleParser(cache));
                        bind(ParseMessageService.class).asEagerSingleton();
                        bind(MessageMetaResource.class).asEagerSingleton();
                    }
                });

        environment.jersey().register(injector.getInstance(MessageMetaResource.class));
    }

    public static void main(String args[]) {
        try {
            new MessageMetaApplication().run(args);
        } catch (Exception e) {
            log.error("Unable to start application.", e);
        }
    }
}
