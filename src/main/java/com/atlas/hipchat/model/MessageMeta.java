package com.atlas.hipchat.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Set;

/**
 * Wrapper class for message meta information.
 */

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageMeta {

    private Set<String> mentions;
    private Set<String> emoticons;
    private Set<Link> links;
}
