package com.atlas.hipchat.rest;


import com.atlas.hipchat.model.MessageMeta;
import com.atlas.hipchat.service.ParseMessageService;
import com.google.inject.Inject;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Rest APIs for Message interactions.
 */

@Slf4j
@Path("/v1/messages/")
@Produces(MediaType.APPLICATION_JSON)
public class MessageMetaResource {

    ParseMessageService parseMessageService;

    @Inject
    public MessageMetaResource(ParseMessageService parseMessageService) {
        this.parseMessageService = parseMessageService;
    }

    /**
     * Parses the given messgae as POST body input and return a JSON of meta data with possible mentions, emoticons and links
     * @param message
     * @return MessageMeta
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    public Response message(String message) {

        MessageMeta meta = parseMessageService.parse(message);
        return Response.ok().entity(meta).build();
    }


}
